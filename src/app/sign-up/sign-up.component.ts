import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../auth-service';
import { FormGroup } from '@angular/forms';
import { DataStorageService } from '../data-storage-service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  catagory: string;
  constructor(private route: ActivatedRoute, private auth: AuthService, private storage: DataStorageService, private router: Router) { }

  ngOnInit() {
      this.route.params.subscribe(
           (param: Params) => {
               this.catagory = param['category'];
           }
      );
  }

  onSubmit(form: FormGroup) {
      const name = form.value.name;
      const email = form.value.email;
      const password = form.value.password;
      const phoneNo = form.value.phoneNo;
      const gender = form.value.gender;
      const age = form.value.age;
      if( this.catagory == "admin") {
          this.storage.signUpAdmin(name, email, password, phoneNo, gender, age).subscribe(
              (data) => {
                   if(data == false) {
                      this.router.navigate(['../','signUp','admin']);
                   }
                   else {
                      this.router.navigate(['../','signIn','admin']);
                   }
              }
          );
      }
      else {
          this.storage.signUpUser(name, email, password, phoneNo, gender, age).subscribe(
            (data) => {
                if(data == false) {
                    this.router.navigate(['../','signUp','user']);
                 }
                 else {
                    this.router.navigate(['../','signIn','user']);
                 }
            }
          );
      }
  }

}
