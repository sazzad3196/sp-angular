import { Component, OnInit } from '@angular/core';
import { ItemData } from '../itemData';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { AuthService } from '../auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vegetable',
  templateUrl: './vegetable.component.html',
  styleUrls: ['./vegetable.component.css']
})
export class VegetableComponent implements OnInit {

  vegetableData: ItemData[];
  constructor(private auth: AuthService, private router: Router, private allItemService: AllItemService, private dataStorage: DataStorageService) { }

  ngOnInit() {  
       this.allItemService.cleanVegetableData(); 
       this.dataStorage.getAnyItem("vegetables").subscribe(
             (vegetableData: ItemData[]) => {
                  for(let vegetable of vegetableData) {
                      this.allItemService.addVegetableData(vegetable);
                  }
                  // console.log("Count: " + this.count);
                  this.vegetableData = this.allItemService.getVegetableData();
             }
       ); 
  }

  onClick(id: number) {
    //  console.log("IIIDD: " + id);
     if(this.auth.userToken == null) {
         this.router.navigate(['/signIn/user']);
     }
     else {
         this.router.navigate(['/itemAdded/' + id]);
     }
  }
}
