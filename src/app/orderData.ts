import { UserData } from './userData';
import { CardData } from './cardData';


export class OrderData {
    public id: number;
    public userId: number;
    public cardId: number;
    public address: string;
    public date: string;
    public city: string;
    public userData: UserData;
    public cardData: CardData;

    constructor(id: number, userId: number, cardId: number, address: string, date: string, city: string, userData: UserData, cardData: CardData) {
          this.id = id;
          this.userId = userId;
          this.cardId = cardId;
          this.address = address;
          this.date = date;
          this.city = city;
          this.cardData = cardData;
          this.userData = userData;
    }
}