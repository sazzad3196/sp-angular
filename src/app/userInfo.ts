export class UserInfo {
    id: number;
    name: string;
    password: string;
    email: string;
    phoneNo: string;
    gender: string;
    age: number;

    UserInfo(id: number, name: string, password: string, email: string, phoneNo: string, gender: string, age: number) {
         this.id = id;
         this.name = name;
         this.password = password;
         this.email = email;
         this.phoneNo = phoneNo;
         this.gender = gender;
         this.age = age;
    }
}