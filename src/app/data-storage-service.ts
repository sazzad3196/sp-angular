import {  OnInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AllItemService } from './all.item.service';
import { AuthService } from './auth-service';


@Injectable()
export class DataStorageService implements OnInit{

     constructor(private http: HttpClient, private allItemService: AllItemService, private allItem: AllItemService) {}
     ngOnInit() {}


    getAnyItem(item: String) {
        // return this.http.get('http://localhost:8080/SP/bread').subscribe(
        //       (breadData: BreadData[]) => {
        //           console.log(JSON.stringify(breadData));
        //           this.breadItem.setBreadData(breadData);
        //       }
        // );
        return this.http.get('http://localhost:8080/SP/shoppingList?item=' + item);
    }

    checkSignInAdmin(email: string, password: string) {
         return this.http.get('http://localhost:8080/SP/adminSignIn?email=' + email + "&password=" + password);
    }

    checkSignInUser(email: string, password: string) {
         return this.http.get('http://localhost:8080/SP/userSignIn?email=' + email + "&password=" + password);
    }

    signUpAdmin(name: string, email: string, password: string, phoneNo: string, gender: string, age: number) {
         console.log("Gender: " + gender);
         console.log("Name: " + name);
         return this.http.post('http://localhost:8080/SP/adminSignUp', {
             "name" : name,
             "email" : email,
             "password" : password,
             "phoneNo" : phoneNo,
             "gender" : gender,
             "age" : age,
             "catagory" : "admin"

         });
    }

    signUpUser(name: string, email: string, password: string, phoneNo: string, gender: string, age: number) {
        return this.http.post('http://localhost:8080/SP/userSignUp', {
             "name" : name,
             "email" : email,
             "password" : password,
             "phoneNo" : phoneNo,
             "gender" : gender,
             "age" : age,
             "catagory" : "user"

         });
    }

    uploadImage(formData: FormData) {
         return this.http.post('http://localhost:8080/SP/uploadImage', formData);
    }

    addNewShoppingList(name: string, imageName: string, price: number, catagory: string) {
         return this.http.post('http://localhost:8080/SP/shoppingList', {
            "name" : name,
            "price" : price,
            "catagory" : catagory,
            "imageName" : imageName,
            "item" : "add"
         });
    }

    editShoppingList(name: string, imageName: string, price: number, catagory: string, id: number) {
     return this.http.post('http://localhost:8080/SP/shoppingList', {
        "name" : name,
        "price" : price,
        "catagory" : catagory,
        "imageName" : imageName,
        "item" : "edit",
        "id" : id
     });
}

    getAllProduct() {
         const item = "all";
         return this.http.get('http://localhost:8080/SP/shoppingList?item=' + item);
    }

    deleteProduct(id: number) {
        return this.http.get('http://localhost:8080/SP/shoppingList?item=delete&id=' + id);
    }

    deleteCardInfo(id: number) {
        return this.http.get('http://localhost:8080/SP/shoppingList?item=deleteCard&id=' + id);
    }

    setCardInformation(userId: number, itemId: number, quantity: number) {
         return this.http.post('http://localhost:8080/SP/shoppingList', {
             "userId" : userId,
              "itemId" : itemId,
              "quantity" : quantity,
              "item" : "card"
         });
    }

    setOrderInfo(cardId: number, userId: number, address: string, city: string, date: string) {
          return this.http.post('http://localhost:8080/SP/shoppingList', {
               "cardId": cardId,     
               "userId" : userId,
               "address" : address,
               "city" : city,
               "date" : date,
               "item" : "order"
          });
    }

    getCardInfo(userId: number, status: number) {
        return this.http.get('http://localhost:8080/SP/shoppingList?userId=' + userId + "&item=card&status=" + status);
    }

    getSingleCardInfo(cardId: number) {
       return this.http.get('http://localhost:8080/SP/shoppingList?cardId=' + cardId + "&item=singleCard");
    }

    getOrderInfo() {
         return this.http.get('http://localhost:8080/SP/shoppingList?item=order');
    }

    getAllOrderInfo() {
        return this.http.get('http://localhost:8080/SP/shoppingList?item=manageOrder');
    }

    deleteOrderInfo(orderId: number) {
        return this.http.get('http://localhost:8080/SP/shoppingList?item=deleteOrder&id=' + orderId);
    }




}