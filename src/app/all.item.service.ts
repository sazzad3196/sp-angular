import { ItemData } from './itemData';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { UserInfo } from './userInfo';
import { CardInfo } from './cardInfo';

@Injectable()

export class AllItemService {
     changedBread = new Subject<ItemData[]>();
     changedFruits = new Subject<ItemData[]>();
     
     private allProductData: ItemData[] = [];
     private breadData: ItemData[] = [];
     private fruitsData: ItemData[] = [];
     private dairyData: ItemData[] = [];
     private vegetableData: ItemData[] = [];
     private cardInfo: CardInfo[] = [];
     private userInfo: UserInfo;
     private countCardQuantity = 0;
     
     getCountCardQuantity() {
         for(let cardInfo of this.cardInfo) {
              console.log("KKKKK");
              this.countCardQuantity = this.countCardQuantity + cardInfo.quantity;
         }
         return this.countCardQuantity;
     }

     setCardInfo(cardInfo: CardInfo) {
          this.cardInfo.push(cardInfo);
     }

     getCardInfo() {
          return this.cardInfo;
     }

     cleanCardInfo() {
         this.cardInfo = [];
     }

     getUserInfo() {
        return this.userInfo;
     }

     setUserInfo(userInfo: UserInfo) {
         this.userInfo = userInfo;
         // console.log("Email: " + this.userInfo.email);
     }

     cleanAllProductData() {
        this.allProductData = [];
     }
     addAllProductData(itemData: ItemData) {
          this.allProductData.push(itemData);    
     }

     getProductData(id: number) {
        for(let allProductData of this.allProductData) {
                if(allProductData.id == id){
                    return allProductData;
                }
         }
     }
     getAllProductData() {
         return this.allProductData.slice();
     }



     addBreadData(breadData: ItemData) {
         this.breadData.push(breadData);
         this.changedBread.next(this.breadData.slice());      
     }
     cleanBreadData() {
        this.breadData = [];
     }
     getBreadData() {
        // for(let breadData of this.breadData) {
        //     console.log("Id: " + breadData.id);
        //     console.log("Name: " + breadData.name);
        // }
        return this.breadData.slice();
     }



     addFruitsData(fruitData: ItemData) {
        this.fruitsData.push(fruitData);
        this.changedFruits.next(this.breadData.slice());
        
     }
     getFruitsData() {
        // for(let fruits of this.fruitsData) {
        //     console.log("Id: " + fruits.id);
        //     console.log("Name: " + fruits.name);
        // }
        return this.fruitsData.slice();
     }

     cleanFruitsData() {
         this.fruitsData = [];
     }



     addDairyData(dairyData: ItemData) {
        this.dairyData.push(dairyData);  
     }
     getDairyData() {
        return this.dairyData.slice();
     }
     cleanDairyData() {
        this.dairyData = [];
     }



     addVegetableData(vegetableData: ItemData) {
        this.vegetableData.push(vegetableData);   
     }
     getVegetableData() {
        return this.vegetableData.slice();
     }
     cleanVegetableData() {
         this.vegetableData = [];
     }



}