import { Component, OnInit } from '@angular/core';
import { AllItemService } from '../all.item.service';
import { UserInfo } from '../userInfo';
import { CardInfo } from '../cardInfo';
import { DataStorageService } from '../data-storage-service';
import { NgForm, FormGroup } from '@angular/forms';
import { AuthService } from '../auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {

  cardInfo: CardInfo[];
  userInfo: UserInfo;
  countQuantity = 0;
  totalPrice = 0;
  data = false;

  constructor(private router: Router, private allItem: AllItemService, private dataStorage: DataStorageService, private auth: AuthService) { }

  ngOnInit() {
      this.countQuantity = this.allItem.getCountCardQuantity();
      this.userInfo = this.allItem.getUserInfo();
      this.dataStorage.getCardInfo(this.userInfo.id, 1).subscribe(
           (cardInfo: CardInfo[]) => {
                this.cardInfo = cardInfo;
                for(let cardInfo of this.cardInfo) {
                    this.countQuantity = this.countQuantity + cardInfo.quantity;
                    this.totalPrice = this.totalPrice + (cardInfo.quantity * cardInfo.shoppingListData.price);
                }
           }
       );
  }

  onSubmit(form: FormGroup) {
      let address = form.value.address;
      let city = form.value.city;
      let date = form.value.date;
      for(let cardInfo of this.cardInfo) {
            this.dataStorage.setOrderInfo(cardInfo.id, this.userInfo.id, address, city, date).subscribe(
                (data) => {
                    if(data == true) {
                        this.dataStorage.getCardInfo(this.userInfo.id, 1).subscribe(
                            (cardInfo: CardInfo[]) => {
                                this.cardInfo = cardInfo;
                                if(this.cardInfo.length == 0) {
                                    this.auth.card = false;
                                    this.router.navigate(['/confirm']);
                                }
                            }
                         );
                    }
                }
            );
      }

  }

}
