import { Component, OnInit } from '@angular/core';
import { CardInfo } from '../cardInfo';
import { Router, NavigationEnd } from '@angular/router';
import { AllItemService } from '../all.item.service';
import { UserInfo } from '../userInfo';
import { DataStorageService } from '../data-storage-service';
import { AuthService } from '../auth-service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  cardInfo: CardInfo[];
  cardInfo1: CardInfo[];
  userInfo: UserInfo;
  constructor(private auth: AuthService, private router: Router, private allItem: AllItemService, private dataStorage: DataStorageService) {
       this.router.events.subscribe((e: any) => {
             if(e instanceof NavigationEnd) {
                 this.cardInfo = this.allItem.getCardInfo();
                 this.userInfo = this.allItem.getUserInfo();
             }
       });
   }

  ngOnInit() {
      this.cardInfo = this.allItem.getCardInfo();
      this.userInfo = this.allItem.getUserInfo();
  }

  onCheckOut() {
      this.router.navigate(['/checkOut']);
  }

  onClick(id: number) {
      return this.dataStorage.deleteCardInfo(id).subscribe(
           (data) => {
               if(data == true) {
                   this.dataStorage.getCardInfo(this.userInfo.id, 1).subscribe(
                        (cardInfo: CardInfo[]) => {
                            this.cardInfo1 = cardInfo;
                            if(this.cardInfo1.length == 0) {
                                 this.auth.card = false;
                                 this.router.navigate(['/allCatagories']);
                            }
                            else{
                                 this.allItem.cleanCardInfo();
                                 for(let cardInfo of this.cardInfo1) {
                                     this.allItem.setCardInfo(cardInfo);
                                 }
                                 this.router.navigate(['/card']);
                            }
                        }
                   );
               }
           }
      );
  }

}
