import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../data-storage-service';
import { OrderData } from '../orderData';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-manage-order',
  templateUrl: './manage-order.component.html',
  styleUrls: ['./manage-order.component.css']
})
export class ManageOrderComponent implements OnInit {

  orderData: OrderData[];
  constructor(private dataStorage: DataStorageService, private router: Router) {
        this.router.events.subscribe((e: any) => {
             if(e instanceof NavigationEnd) {
                  this.allOrderInfo();
             }
        });
   }

  ngOnInit() {
       this.allOrderInfo();
  }

  allOrderInfo() {
     this.dataStorage.getAllOrderInfo().subscribe(
          (orderData: OrderData[]) => {
              this.orderData = orderData;
          }
     );
  }

  onView(orderId: number) {
       this.router.navigate(['/viewManageOrder/' + orderId]);
  }

  onDelete(orderId: number) {
      this.dataStorage.deleteOrderInfo(orderId).subscribe(
           (data) => {
                if(data == true) {
                     this.router.navigate(['/manageOrder']);
                }
                else {
                     alert("Item not deleted.Error occur!");
                }
           }
      );
  }

}
