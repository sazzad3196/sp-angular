import { DataStorageService } from './data-storage-service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserInfo } from './userInfo';
import { AllItemService } from './all.item.service';

export class AuthService {
    allowEdit = false;
    token: string;
    userToken: string;
    adminToken: string;
    admin = false;
    user = false;
    logout = false;
    card = false;
    constructor(private storage: DataStorageService, private router: Router, private route: ActivatedRoute, private allItem: AllItemService)  {}
    
    isAuthenticated() {
        return this.token != null;
    }

    isUserAuthenticated() {
        return this.userToken != null;
    }

    isAdminAuthenticated() {
        return this.adminToken != null;
    }

    onLogout() {
        this.logout = false;
        this.allowEdit = false;
        this.userToken = null;
        this.adminToken = null;
        this.admin = false;
        this.user = false;
        this.token = null;
    }

    signInUser(email: string, password: string) {
        this.storage.checkSignInUser(email, password).subscribe(
            (userInfo: UserInfo) => {
                if( userInfo != null) {
                    this.adminToken = null;
                    this.logout = true;
                    this.userToken = "user";
                    this.token = '';
                    this.admin = true;
                    this.allowEdit = false;
                    this.allItem.setUserInfo(userInfo);
                    this.router.navigate(['/allCatagories']);
                }
                else {
                    this.router.navigate(['../', 'signIn', 'user']);
                }
            }
        );
    }

    signInAdmin(email: string, password: string) {
        this.storage.checkSignInAdmin(email, password).subscribe(
            (value) => {
                if( value == true) {
                    this.logout = true;
                    this.userToken = null;
                    this.adminToken = "admin";
                    this.user = true;
                    this.token = '';
                    this.allowEdit = false;
                    this.router.navigate(['/allCatagories']);
                }
                else {
                    this.router.navigate(['../','signIn','admin']);
                }
            }
        );
    }

}