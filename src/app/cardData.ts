import { ShoppingListData } from './shoppingListData';

export class CardData{
    id: number;
    userId: number;
    itemId: number;
    quantity: number;
    shoppingListData: ShoppingListData;

    constructor(id: number, userId: number, itemId: number, quantity: number, shoppingListData: ShoppingListData) {
        this.id = id;
        this.userId = userId;
        this.itemId = itemId;
        this.quantity = quantity;
        this.shoppingListData = shoppingListData;
    }
}