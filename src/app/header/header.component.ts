import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth-service';
import { DataStorageService } from '../data-storage-service';
import { AllItemService } from '../all.item.service';
import { ItemData } from '../itemData';
import { UserInfo } from '../userInfo';
import { CardInfo } from '../cardInfo';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userId: number;
  allowEdit = false;
  person: string;
  itemData: ItemData;
  userInfo: UserInfo;
  cardInfo: CardInfo[];
  constructor(private router: Router, private auth: AuthService, private dataStorage: DataStorageService, private allItem: AllItemService) {
    this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
         this.onLoadItem();
      }
    });
   }

  ngOnInit() {
     this.onLoadItem();
  }

  onLoadItem() {
    this.allItem.cleanCardInfo();
    this.userInfo = this.allItem.getUserInfo();
    if(this.userInfo != null) {
        this.userId = this.userInfo.id;
        this.dataStorage.getCardInfo(this.userId, 1).subscribe(
            (cardInfo: CardInfo[]) => {
                  this.cardInfo = cardInfo;
                  for(let cardInfo of this.cardInfo) {
                        this.auth.card = true;
                        this.allItem.setCardInfo(cardInfo);
                  }     
            }
        );
    }
  }

  onUser() {
     this.auth.allowEdit = true;
     this.person = "user";
     
  }

  onAdmin() {
     this.auth.allowEdit = true;
     this.person = "admin";
  }

  onLogout() {
      this.auth.onLogout();
  }

  onCard() {
      this.router.navigate(['/card']);
  }

}
