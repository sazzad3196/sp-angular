import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllCatagoriesComponent } from './all-catagories/all-catagories.component';
import { BreadComponent } from './bread/bread.component';
import { DairyComponent } from './dairy/dairy.component';
import { FruitsComponent } from './fruits/fruits.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ManageProductsComponent } from './manage-products/manage-products.component';
import { NewProductsComponent } from './new-products/new-products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { VegetableComponent } from './vegetable/vegetable.component';
import { ItemAddedComponent } from './item-added/item-added.component';
import { CardComponent } from './card/card.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { MyOrderComponent } from './my-order/my-order.component';
import { ViewMyOrderComponent } from './view-my-order/view-my-order.component';
import { ViewManageOrderComponent } from './view-manage-order/view-manage-order.component';
import { ManageOrderComponent } from './manage-order/manage-order.component';

const appRoutes: Routes = [
    {path: '', redirectTo: "/allCatagories", pathMatch: 'full'},
    {path: "allCatagories", component: AllCatagoriesComponent, children: [
        {path: 'bread', component: BreadComponent},
        {path: 'dairy', component: DairyComponent},
        {path: 'fruits', component: FruitsComponent},
        {path: 'vegetable', component: VegetableComponent}
        

    ]},
    {path: "signIn/:category", component: SignInComponent},
    {path: "signUp/:category", component: SignUpComponent},
    {path: "manageProducts", component: ManageProductsComponent},
    {path: "newProducts", component: NewProductsComponent},
    {path: 'manageProducts/:id', component: EditProductComponent},
    {path: 'itemAdded/:id', component: ItemAddedComponent},
    {path: 'card', component: CardComponent},
    {path: 'checkOut', component: CheckOutComponent},
    {path: 'confirm', component: ConfirmComponent},
    {path: 'myOrder', component: MyOrderComponent},
    {path: 'viewMyOrder/:id', component: ViewMyOrderComponent},
    {path: 'manageOrder', component: ManageOrderComponent},
    {path: 'viewManageOrder/:id', component: ViewManageOrderComponent}

]
@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' })],
    exports: [RouterModule]
})
export class SPRounting{
    
}