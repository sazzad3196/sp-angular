import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AllItemService } from '../all.item.service';
import { UserInfo } from '../userInfo';
import { ItemData } from '../itemData';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataStorageService } from '../data-storage-service';

@Component({
  selector: 'app-item-added',
  templateUrl: './item-added.component.html',
  styleUrls: ['./item-added.component.css']
})
export class ItemAddedComponent implements OnInit {

  itemId: number;
  userId: number;
  userInfo: UserInfo;
  name: string;
  image: string;
  price: number;
  catagory: string;
  quantity: number;
  itemData: ItemData;
  productForm: FormGroup;
  imgURL: string;
  constructor(private router: Router, private route: ActivatedRoute, private allItem: AllItemService, private dataStorage: DataStorageService) { }

  ngOnInit() {
       this.userInfo = this.allItem.getUserInfo();
       this.userId = this.userInfo.id;
       this.route.params.subscribe(
           (param: Params) => {
               this.itemId = param['id'];
               this.itemData = this.allItem.getProductData(this.itemId);
               this.name = this.itemData.name;
               this.price = this.itemData.price;
               this.image = this.itemData.image;
               this.catagory = this.itemData.catagory;
               this.imgURL = "http://localhost:8080/SP/images/"+ this.image;
            //    console.log("Catagory: " + this.catagory);
               this.initForm();
           }
       );
  }

  initForm() {
      this.productForm = new FormGroup({
           'name': new FormControl(this.name),
           'price': new FormControl(this.price),
           'catagory': new FormControl(this.catagory),
           'quantity': new FormControl(null)
      });
      this.productForm.controls['name'].disable();
      this.productForm.controls['price'].disable();
      this.productForm.controls['catagory'].disable();
  }

  onSubmit() {
       this.quantity = this.productForm.value['quantity'];
       this.dataStorage.setCardInformation(this.userId, this.itemId, this.quantity).subscribe(
            (data) => {
                if(data == true) {
                     if(this.catagory == "bread") {
                         this.router.navigate(['/allCatagories/bread']);
                     }
                     else if(this.catagory == "fruits") {
                         this.router.navigate(['/allCatagories/fruits']);
                     }
                     else if(this.catagory == "vegetable") {
                         this.router.navigate(['/allCatagories/vegetable']);
                     } 
                     else if(this.catagory == "dairy") {
                        this.router.navigate(['/allCatagories/dairy']);
                     } 
                     else {
                         this.router.navigate(['/allCatagories/dairy']);
                     }
                }
                else{
                    alert("This data not added in database!!!Error occur.");
                }
            }
       );
  }

}
