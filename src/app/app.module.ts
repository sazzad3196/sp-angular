import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AllCatagoriesComponent } from './all-catagories/all-catagories.component';
import { BreadComponent } from './bread/bread.component';
import { DairyComponent } from './dairy/dairy.component';
import { FruitsComponent } from './fruits/fruits.component';
import { SPRounting } from './sp-routing.module';
import { AllItemService } from './all.item.service';
import { HttpClientModule } from '@angular/common/http';
import { DataStorageService } from './data-storage-service';
import { AuthService } from './auth-service';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { HeaderComponent } from './header/header.component';
import { ManageProductsComponent } from './manage-products/manage-products.component';
import { NewProductsComponent } from './new-products/new-products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { VegetableComponent } from './vegetable/vegetable.component';
import { ItemAddedComponent } from './item-added/item-added.component';
import { UserInfo } from './userInfo';
import { CardInfo } from './cardInfo';
import { CardComponent } from './card/card.component';
import { ShoppingListData } from './shoppingListData';
import { CheckOutComponent } from './check-out/check-out.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { MyOrderComponent } from './my-order/my-order.component';
import { ViewMyOrderComponent } from './view-my-order/view-my-order.component';
import { ManageOrderComponent } from './manage-order/manage-order.component';
import { ViewManageOrderComponent } from './view-manage-order/view-manage-order.component';

@NgModule({
  declarations: [
    AppComponent,
    AllCatagoriesComponent,
    BreadComponent,
    DairyComponent,
    FruitsComponent,
    SignInComponent,
    SignUpComponent,
    HeaderComponent,
    ManageProductsComponent,
    NewProductsComponent,
    EditProductComponent,
    VegetableComponent,
    ItemAddedComponent,
    CardComponent,
    CheckOutComponent,
    ConfirmComponent,
    MyOrderComponent,
    ViewMyOrderComponent,
    ManageOrderComponent,
    ViewManageOrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SPRounting,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [AllItemService, DataStorageService, AuthService, UserInfo, CardInfo, ShoppingListData],
  bootstrap: [AppComponent]
})
export class AppModule { }
