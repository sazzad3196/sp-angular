export class ShoppingListData {
    public id: number;
    public name: string;
    public price: number;
    public image: string;
    public catagory: string;

    constructor(id: number, name: string, price: number, image: string, catagory: string) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.catagory = catagory;
    }
}