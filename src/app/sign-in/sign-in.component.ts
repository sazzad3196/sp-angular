import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth-service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  catagory: string;
  constructor(private route: ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
      this.route.params.subscribe(
           (param: Params) => {
               this.catagory = param['category'];
           }
      );
  }

  onSubmit(form: FormGroup) {
       const email = form.value.email;
       const password = form.value.password;
       console.log("Email: " + email);
       console.log("Password: " + password);
       if(this.catagory == "user") {
           this.auth.signInUser(email, password);
       }
       else {
          this.auth.signInAdmin(email, password);
       }
  }

}
