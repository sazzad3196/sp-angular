import { Component, OnInit } from '@angular/core';
import { ItemData } from '../itemData';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { AuthService } from '../auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dairy',
  templateUrl: './dairy.component.html',
  styleUrls: ['./dairy.component.css']
})
export class DairyComponent implements OnInit {

  dairyData: ItemData[];
  constructor(private router: Router, private auth: AuthService, private allItemService: AllItemService, private dataStorage: DataStorageService) { }

  ngOnInit() {  
       this.allItemService.cleanDairyData(); 
       this.dataStorage.getAnyItem("dairy").subscribe(
             (dairyData: ItemData[]) => {
                  for(let dairy of dairyData) {
                      this.allItemService.addDairyData(dairy);
                  }
                  this.dairyData = this.allItemService.getDairyData();
             }
       ); 
  }

  onClick(id: number) {
    //  console.log("IIIDD: " + id);
     if(this.auth.userToken == null) {
         this.router.navigate(['/signIn/user']);
     }
     else {
         this.router.navigate(['/itemAdded/' + id]);
     }
  }

}
