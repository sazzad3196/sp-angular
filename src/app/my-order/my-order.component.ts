import { Component, OnInit } from '@angular/core';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { UserInfo } from '../userInfo';
import { OrderData } from '../orderData';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.css']
})
export class MyOrderComponent implements OnInit {
   userInfo: UserInfo;
   orderData: OrderData[];

  constructor(private allItem: AllItemService, private dataStorage: DataStorageService, private router: Router) { }

  ngOnInit() {
      this.userInfo = this.allItem.getUserInfo();
      this.dataStorage.getOrderInfo().subscribe(
          (orderData: OrderData[]) => {
              this.orderData = orderData;
          }
      );
  }

  onView(cardId: number) {
      this.router.navigate(['/viewMyOrder/' + cardId]);
  }

}
