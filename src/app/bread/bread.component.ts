import { Component, OnInit } from '@angular/core';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { ItemData } from '../itemData';
import { AuthService } from '../auth-service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-bread',
  templateUrl: './bread.component.html',
  styleUrls: ['./bread.component.css']
})
export class BreadComponent implements OnInit {

  breadData: ItemData[];
  index = 0;
  productForm: FormGroup;

  constructor(private router: Router,private allItemService: AllItemService, private dataStorage: DataStorageService, private auth: AuthService) { }

  ngOnInit() {   
       this.allItemService.cleanBreadData();
       this.dataStorage.getAnyItem("bread").subscribe(
             (breadData: ItemData[]) => {
                  for(let bread of breadData) {
                      this.allItemService.addBreadData(bread);
                  }
                  this.breadData = this.allItemService.getBreadData();
                  // console.log("name: " + this.breadData[2].name);
                  // for(let bread of this.breadData) {
                  //     this.index = this.index + 1;
                  // }
             }
       ); 
  }

  onClick(id: number) {
    //  console.log("IIIDD: " + id);
     if(this.auth.userToken == null) {
         this.router.navigate(['/signIn/user']);
     }
     else {
         this.router.navigate(['/itemAdded/' + id]);
     }
  }

}
