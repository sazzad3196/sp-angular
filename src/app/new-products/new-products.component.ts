import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataStorageService } from '../data-storage-service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-products',
  templateUrl: './new-products.component.html',
  styleUrls: ['./new-products.component.css']
})
export class NewProductsComponent implements OnInit {

  imgURL: any;
  imageName: string;
  selectedFile: File; 

  constructor(private dataStorage: DataStorageService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  preview(event) { 
      this.selectedFile = event.target.files[0];
      const formData = new FormData();
      formData.append("file", this.selectedFile);
      console.log(this.selectedFile);
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (_event) => {
           this.imgURL = reader.result;
      }
      this.dataStorage.uploadImage(formData).subscribe(
        (data: string) => {
            if(data != null) {
                this.imageName = data;
            }
            else{ 
                console.log("Error");
            }
        }
   );
      

  }

  onSubmit(form: FormGroup) {
       const name = form.value.name;
       const price = form.value.price;
       const catagory = form.value.catagory;
       this.dataStorage.addNewShoppingList(name, this.imageName, price, catagory).subscribe(
             (data) => {
                if(data == true) {
                    this.router.navigate(['/manageProducts']);
                }
                else {
                    console.log("Error");
                }
             }
       );
  }

}
