import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ItemData } from '../itemData';
import { AllItemService } from '../all.item.service';
import { NgForm, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DataStorageService } from '../data-storage-service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  id: number;
  name: string;
  image: string;
  price: number;
  catagory: string;
  imgURL: any;
  itemData: ItemData;
  productForm: FormGroup;
  selectedFile: File;
  formBuilder: FormBuilder;
  constructor(private router: Router, private route: ActivatedRoute, private allItem: AllItemService, private dataStorage: DataStorageService) { }

  ngOnInit() {
      this.route.params.subscribe(
          (param: Params) => {
              this.id = param['id'];
              console.log("Id: " + this.id);
              this.itemData = this.allItem.getProductData(this.id);
              this.image = this.itemData.image;
              this.name = this.itemData.name;
              this.price = this.itemData.price;
              this.catagory = this.itemData.catagory;
              this.imgURL = "http://localhost:8080/SP/images/"+ this.image;
              this.initForm();
          }
      );
  }

  onSubmit() {
     const name = this.productForm.value['name'];
     const price = this.productForm.value['price'];
     const catagory = this.productForm.value['catagory'];
     this.dataStorage.editShoppingList(name, this.image, price, catagory, this.id).subscribe(
          (data) => {
               if(data == true) {
                   this.router.navigate(['/manageProducts']);
               }
          }
     );
       
  }

  private initForm() {
      this.productForm = new FormGroup({
           'name': new FormControl(this.name, Validators.required),
           'price': new FormControl(this.price, Validators.required),
           'uploadImage': new FormControl(''),
           'catagory': new FormControl(this.catagory, Validators.required),
      });
  }

  preview(event) {  
      this.selectedFile = event.target.files[0];
      console.log("Image: " + this.selectedFile.name);
      this.image = this.selectedFile.name;
      this.productForm.controls['uploadImage'].setValue(this.selectedFile ? this.selectedFile : '');
      if(this.selectedFile == null) {
        let fileName = '';
        this.productForm = this.formBuilder.group({
            uploadImage: [fileName, Validators.required]
        });
      }
      

      const formData = new FormData();
      formData.append("file", this.selectedFile);
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (_event) => {
          this.imgURL = reader.result;
      }


      this.dataStorage.uploadImage(formData).subscribe(
        (data: string) => {
            if(data != null) {
                this.image = data;
            }
            else{ 
                console.log("Error");
            }
        }
      );
  }


}
