import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../data-storage-service';
import { ItemData } from '../itemData';
import { AllItemService } from '../all.item.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css']
})
export class ManageProductsComponent implements OnInit {

  itemData: ItemData[];
  constructor(private dataStorage: DataStorageService, private allItem: AllItemService, private router: Router, private route: ActivatedRoute) {
    this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
         console.log('Router event:', e);
         this.onLoadItem();
      }
    });
  }

  ngOnInit() {
      this.onLoadItem();
  }

  onLoadItem() {
    this.dataStorage.getAllProduct().subscribe(
      (itemData: ItemData[]) => {
           this.itemData = itemData;
           for(let itemData of this.itemData) {
               this.allItem.addAllProductData(itemData);
           }
      }
    );
  }

  onDeleteItem(id: number) {
      console.log("Id: " + id);
      this.dataStorage.deleteProduct(id).subscribe(
          (data) => {
            //this.router.navigateByUrl('/', {skipLocationChange: true})
            //.then(() =>
            //  this.router.navigate(['/manageProducts'])
            //);

            this.router.navigate(['/manageProducts']);
          }
      );
  }

  onClick(id: number) {
      this.router.navigate(['/manageProducts/' + id]);
  }



}
