import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManageOrderComponent } from './view-manage-order.component';

describe('ViewManageOrderComponent', () => {
  let component: ViewManageOrderComponent;
  let fixture: ComponentFixture<ViewManageOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewManageOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManageOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
