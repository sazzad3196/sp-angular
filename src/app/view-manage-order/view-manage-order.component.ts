import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { OrderData } from '../orderData';

@Component({
  selector: 'app-view-manage-order',
  templateUrl: './view-manage-order.component.html',
  styleUrls: ['./view-manage-order.component.css']
})
export class ViewManageOrderComponent implements OnInit {

  orderId: number;
  orderData: OrderData[];
  name: string;
  price: number;
  quantity: number;
  image: string;
  address: string;
  city: string;
  constructor(private route: ActivatedRoute, private allItem: AllItemService, private dataStorage: DataStorageService, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (param: Params) => {
          this.orderId = param['id'];
          this.dataStorage.getAllOrderInfo().subscribe(
              (orderData: OrderData[]) => {
                  this.orderData = orderData;
                  if(this.orderData.length > 0) {
                      for(let orderData of this.orderData) {
                          if(orderData.id == this.orderId) {
                               this.address = orderData.address;
                               this.city =  orderData.city;
                               this.name = orderData.cardData.shoppingListData.name;
                               this.price = orderData.cardData.quantity * orderData.cardData.shoppingListData.price;
                               this.quantity = orderData.cardData.quantity;
                               this.image = "http://localhost:8080/SP/images/" + orderData.cardData.shoppingListData.image;
                          }
                      }
                  }
              }
          );
      }
   );
       
  }

}
