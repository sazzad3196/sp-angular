import { Component, OnInit } from '@angular/core';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { ItemData } from '../itemData';
import { AuthService } from '../auth-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrls: ['./fruits.component.css']
})
export class FruitsComponent implements OnInit {

  fruitsData: ItemData[] = [];
  constructor(private auth: AuthService, private router: Router, private allItemService: AllItemService, private dataStorage: DataStorageService) { }

  ngOnInit() {
       this.allItemService.cleanFruitsData();
       this.dataStorage.getAnyItem("fruits").subscribe(
             (fruitsData: ItemData[]) => {
                  for(let fruits of fruitsData) {
                      this.allItemService.addFruitsData(fruits);
                  }
                  this.fruitsData = this.allItemService.getFruitsData();
             }
       );
  }

  onClick(id: number) {
    //  console.log("IIIDD: " + id);
     if(this.auth.userToken == null) {
         this.router.navigate(['/signIn/user']);
     }
     else {
         this.router.navigate(['/itemAdded/' + id]);
     }
  }

}
