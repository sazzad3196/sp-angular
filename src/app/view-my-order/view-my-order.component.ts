import { Component, OnInit } from '@angular/core';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserInfo } from '../userInfo';
import { CardInfo } from '../cardInfo';

@Component({
  selector: 'app-view-my-order',
  templateUrl: './view-my-order.component.html',
  styleUrls: ['./view-my-order.component.css']
})
export class ViewMyOrderComponent implements OnInit {

  cardId: number;
  userInfo: UserInfo;
  cardInfo: CardInfo;
  name: string;
  quantity: number;
  price: number;
  image: string;
  constructor(private route: ActivatedRoute, private allItem: AllItemService, private dataStorage: DataStorageService, private router: Router) { }

  ngOnInit() {
      this.userInfo = this.allItem.getUserInfo();
      this.route.params.subscribe(
          (param: Params) => {
              this.cardId = param['id'];
              console.log("CardId: " + this.cardId);
              this.dataStorage.getSingleCardInfo(this.cardId).subscribe(
                   (cardInfo: CardInfo) => {
                        this.cardInfo = cardInfo;
                        this.name = this.cardInfo.shoppingListData.name;
                        this.image = "http://localhost:8080/SP/images/" + this.cardInfo.shoppingListData.image;
                        this.price = this.cardInfo.quantity * this.cardInfo.shoppingListData.price;
                        this.quantity = this.cardInfo.quantity;
                        console.log("Img: " + this.image);
                   }
              );
          }
       );
  }

}
