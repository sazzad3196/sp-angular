import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth-service';
import { AllItemService } from '../all.item.service';
import { DataStorageService } from '../data-storage-service';
import { ItemData } from '../itemData';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-all-catagories',
  templateUrl: './all-catagories.component.html',
  styleUrls: ['./all-catagories.component.css']
})
export class AllCatagoriesComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private auth: AuthService, private allItem: AllItemService, private dataStorage: DataStorageService) { }
  
  allProductData: ItemData[];
  catagory: string;
  showOutlet = false;

  onActivate(event : any) {
      this.showOutlet = true;
  }
  
  ngOnInit() {
      this.showOutlet = false;
      this.allItem.cleanAllProductData();
      this.dataStorage.getAnyItem("all").subscribe(
        (allProductData: ItemData[]) => {
            for(let allProduct of allProductData) {
                this.allItem.addAllProductData(allProduct);
            }
            this.allProductData = this.allItem.getAllProductData();
        }
    ); 
  }

  onClick(id: number) {
    //  console.log("IIIDD: " + id);
     if(this.auth.userToken == null) {
         this.router.navigate(['/signIn/user']);
     }
     else {
         this.router.navigate(['/itemAdded/' + id]);
     }
  }


}
